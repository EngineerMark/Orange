﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnit : MonoBehaviour {

    [SerializeField] float maxHealthPoints = 100f;
    float currentHealthPoints = 100f;
    public float moveSpeed;
    public float stoppingDistance;
    public Transform unit;


    // Use this for initialization
    void Start()
    {
        unit = GameObject.FindGameObjectWithTag("Unit").transform;
    }

    public float healthAsPercentage
    {
        get
        {
            return currentHealthPoints / maxHealthPoints;
        }
    }

    public void Movement()
    {
        


    }

    
}
