﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healthbar : MonoBehaviour {


    [SerializeField]float maximumHealth = 100f;
    float currentHealth = 100f;

    public float HealthPercentage
    {
        get
        {
            return currentHealth / maximumHealth;
        }
    }
}
