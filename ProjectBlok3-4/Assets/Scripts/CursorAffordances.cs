﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorAffordances : MonoBehaviour {

    CameraController CC;
    [SerializeField]
    Texture2D walkCursor = null;
    [SerializeField]
    Texture2D enemyCursor = null;
    [SerializeField]
    Texture2D notWalkCursor = null;
    [SerializeField]
    Vector2 cursorHotSpot = new Vector2(96, 96);

	// Use this for initialization
	void Start ()
    {
        CC = GetComponent<CameraController>();
        CC.layerChangeObservers += OnLayerChanged;
	}
	
    private void OnLayerChanged()
    {
        print("Cursor reporting for booty!");
        switch (CC.layerhit)
        {
            case Layer.Walkable:
                Cursor.SetCursor(walkCursor, cursorHotSpot, CursorMode.Auto);
                break;
            case Layer.Enemy:
                Cursor.SetCursor(enemyCursor, cursorHotSpot, CursorMode.Auto);
                break;
            case Layer.RaycastEndStop:
                Cursor.SetCursor(notWalkCursor, cursorHotSpot, CursorMode.Auto);
                break;
            default:
                Debug.LogError("Don't know what cursor to show");
                return;

        }
    }
}
