﻿public enum Layer
{
    Terrain = 8,
    Building = 9,
    Unit = 10,
    Enemy = 11,
    NotWalkable = 12,
    Walkable = 13,
    RaycastEndStop = -1
}
