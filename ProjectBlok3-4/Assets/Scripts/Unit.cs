﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Unit : MonoBehaviour {

    private NavMeshAgent nav;
    private bool moving;
    Ray ray;
    RaycastHit hit;
    
    private float rayLength;
    float currentHealthPoints = 100f;

    //unit stats
    [SerializeField] float maxHealthPoints = 100f;
    public float def;
    public float atk = 10;
    public float attackVar = 2;
    public float attackSpeed = 2.5f;
    public float reach = 3;


    



    // Use this for initialization
    void Start ()
    {
        nav = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update ()
    {
        UnitMovement();
        
    }

    public Unit()
    {
        rayLength = 100;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, hit.point);
        Gizmos.color = Color.black;
    }

    private void UnitMovement()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Input.GetMouseButtonDown(1))
        {
            if(Physics.Raycast(ray, out hit, rayLength))
            {
                nav.destination = hit.point;
            }
        }
        if(nav.remainingDistance <= nav.stoppingDistance)
        {
            moving = false;
        }
        else
        {
            moving = true;
        }
    }
    public float healthAsPercentage
    {
        get
        {
            return currentHealthPoints / maxHealthPoints;
        }
    }

    public enum States {Idle, Moving, Combat, Following }
}
