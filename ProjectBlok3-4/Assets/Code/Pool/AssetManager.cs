﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetManager : Manager {
    private static int ID;
    private Dictionary<int, Pool> pools;
    private Transform pool;

    public AssetManager()
    {
        pools = new Dictionary<int, Pool>();
    }

    public override void Awake()
    {
        base.Awake();

        pool = new GameObject().transform;

        pool.gameObject.name = "PoolParent";
        Pool.PoolObject = pool;
    }

    public int CreatePool(GameObject prefab, int amount = 0)
    {
        for (int i = 0; i < pools.Count; i++)
        {
            if (pools[i].GetOriginal() == prefab)
            {
                if (amount > 0)
                    pools[i].CreateFromPrefab(prefab, amount);
                return i;
            }
        }

        Pool pool = new Pool(prefab)
        {
            PoolID = ID,
        };

        if (amount > 0)
            pool.CreateFromPrefab(prefab, amount);

        pools.Add(ID,pool);
        ID++;
        return pool.PoolID;
    }

    public GameObject GetGameObjectFromPool(int poolID)
    {
        if (!pools.ContainsKey(poolID))
            return null;

        GameObject go = pools[poolID].GetFromPool();
        go.SetActive(true);
        return go;
    }

    public GameObject GetGameObjectFromPool(GameObject prefab)
    {
        for (int i = 0; i < pools.Count; i++)
        {
            if (pools[i].GetOriginal() != prefab)
                continue;

            GameObject go = GetGameObjectFromPool(pools[i].PoolID);
            go.SetActive(true);
            return go;
        }

        return Object.Instantiate(prefab);
    }

    public void AddGameObjectToPool(GameObject go, int poolID = -1)
    {
        if (go == null)
            return;

        if(poolID>=0 && pools.ContainsKey(poolID))
        {
            pools[poolID].AddToPool(go);
            go.transform.parent = pool;
            return;
        }

        Object.Destroy(go);
    }
}
