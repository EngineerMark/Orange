﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool
{
    private Queue<GameObject> pool;
    private GameObject original;
    private int poolID;
    private static Transform poolObject;

    public Pool(GameObject original)
    {
        pool = new Queue<GameObject>();
        this.original = original;
    }

    public void CreateFromPrefab(GameObject go = null)
    {
        if (!go) go = Object.Instantiate(original);

        go.name = original.name;
        Entity e = EntityManager.GetEntity<Entity>(go.GetInstanceID());
        e.PoolID = poolID;
        AddToPool(go);
    }

    public void CreateFromPrefab(GameObject go, int amount)
    {
        if (go == null) return;

        for (int i = 0; i < amount; i++)
            CreateFromPrefab(go);
    }

    // Perhaps make this a get-set accessor in future?
    public GameObject GetOriginal()
    {
        return original;
    }

    public GameObject GetFromPool(GameObject go = null)
    {
        if (pool.Peek() == null) CreateFromPrefab(go);

        go = pool.Dequeue();
        go.transform.parent = null;
        return go;
    }

    public void AddToPool(GameObject go)
    {
        if (go == null) return;

        Entity e = EntityManager.GetEntity<Entity>(go.GetInstanceID());
        if (e.PoolID == -1 || e.PoolID != poolID) return;

        pool.Enqueue(go);
        go.transform.parent = poolObject;
        go.transform.position = Vector3.zero;
        go.SetActive(false);
    }

    public int PoolID
    {
        get { return poolID; }
        set { poolID = value; }
    }

    public static Transform PoolObject
    {
        get { return poolObject; }
        set { poolObject = value; }
    }
}
