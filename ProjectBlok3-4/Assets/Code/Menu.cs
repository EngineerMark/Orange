﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class Menu : MonoBehaviour
{
    //Als player op ESC drukt moet timescale op 0 en moet menu verschijnen.


    public AudioMixer audioMixer;

    public Dropdown resolutionDropdown;

    Resolution[] resolutions;

    [SerializeField]
    GameObject PauseMenu;
    [SerializeField]
    GameObject SettingsMenu;
    [SerializeField]
    GameObject StartMenu;
    [SerializeField]
    GameObject LoadGameMenu;
    [SerializeField]
    GameObject ChooseTeam;


    private void Start()
    {
        //Settings Menu
        #region SettingsMenu
        resolutions = Screen.resolutions;

        //zorgd ervoor dat er geen andere rommel tussen staat.
        resolutionDropdown.ClearOptions();

        //Maakt een nieuwe lijst aan.
        List<string> options = new List<string>();

        int currentResolutionIndex = 0;

        // Berekend welke screen methodes er zijn voor je scherm en zet het in de lijst.
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            //Starts with the right value.
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
                currentResolutionIndex = i;
        }

        //Laat zien dat value is verandert.
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        #endregion
    }

    //Settings Menu
    #region SettingsMenu
    public void SetValume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullscreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        //Get resolutions
        Resolution resolution = resolutions[resolutionIndex];

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void BackButtonSettingsMenu()
    {
        SettingsMenu.SetActive(false);
        StartMenu.SetActive(true);

    }

    #endregion

    //Pause Menu
    #region PauseMenu
    private void Update()
    {
        if (Input.GetButtonDown("PauseMenu"))
        {
            PauseMenu.SetActive(!PauseMenu.activeSelf);
        }
    }

    public void ContinueButton()
    {
        PauseMenu.SetActive(false);
    }

    public void ExitButton()
    {
        SceneManager.LoadScene(0);
    }

    public void SaveButton()
    {

    }

    public void SettingsButton()
    {
        SettingsMenu.SetActive(true);
        PauseMenu.SetActive(false);
    }
    #endregion

    //Start Menu
    #region StartMenu
    public void StartGameButton()
    {
        //SceneManager.LoadScene(1);
        ChooseTeam.SetActive(true);
        StartMenu.SetActive(false);
    }

    public void LoadGameButton()
    {
        StartMenu.SetActive(false);
        LoadGameMenu.SetActive(true);
    }

    public void LoadGameBackButton()
    {
        StartMenu.SetActive(true);
        LoadGameMenu.SetActive(false);
    }

    public void LoadGame1()
    {

    }

    public void LoadGame2()
    {

    }

    public void LoadGame3()
    {

    }

    public void StartMenuToSettingsMenuButton()
    {
        SettingsMenu.SetActive(true);
        StartMenu.SetActive(false);

    }

    public void ExitGameButton()
    {
        Application.Quit();

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
    #endregion

    public void TeamTheCarnigians()
    {
        SceneManager.LoadScene(2);
    }

    public void TeamTheTrevarianEmpire()
    {
        SceneManager.LoadScene(2);
    }
}
