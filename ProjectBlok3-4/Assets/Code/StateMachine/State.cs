﻿public class State<T> {

    // Start the state
    public virtual void Enter(T owner) { }

    // Update the state (called each frame once state started)
    public virtual void Execute(T owner) { }

    // Exit the state
    public virtual void Exit(T owner) { }
}
