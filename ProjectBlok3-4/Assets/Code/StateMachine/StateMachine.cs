﻿public class StateMachine<T>
{
    private State<T> previousState;
    private State<T> currentState;

    T owner;

    public StateMachine(T owner)
    {
        this.owner = owner;
        previousState = null;
        currentState = null;
        owner = default(T);
    }

    public void Update()
    {
        // Update the current state if we have one
        if (currentState != null)
            currentState.Execute(owner);
    }

    public void ChangeState(State<T> newState)
    {
        // Is the state null? return if true
        if (newState == null)
            return;

        // Check if we have a current state and exit it
        if (currentState != null)
            currentState.Exit(owner);

        // Assign the current state to the previous state
        previousState = currentState;

        // Assign the new state to current state
        currentState = newState;

        // Enter the current state
        currentState.Enter(owner);
    }

    public State<T> GetState()
    {
        return currentState;
    }

    public void RevertToPreviousState()
    {
        // Change the state to the old state
        ChangeState(previousState);
    }
}
