﻿// Entity has nothing todo, and idles
public class IdleState<T> : State<T> where T : Entity
{
    public override void Enter(T owner)
    {
        base.Enter(owner);
    }

    public override void Execute(T owner)
    {
        base.Execute(owner);
    }

    public override void Exit(T owner)
    {
        base.Exit(owner);
    }
}

// There's an enemy nearby, so the entity will attack
public class AttackState<T> : State<T> where T : Entity
{
    public override void Enter(T owner)
    {
        base.Enter(owner);
    }

    public override void Execute(T owner)
    {
        base.Execute(owner);
    }

    public override void Exit(T owner)
    {
        base.Exit(owner);
    }
}

// HP is too low, so entity will run away
public class FleeState<T> : State<T> where T : Entity
{
    public override void Enter(T owner)
    {
        base.Enter(owner);
    }

    public override void Execute(T owner)
    {
        base.Execute(owner);
    }

    public override void Exit(T owner)
    {
        base.Exit(owner);
    }
}

// If the entity killed an enemy, a little victory dance?
public class VictoryState<T> : State<T> where T : Entity
{
    public override void Enter(T owner)
    {
        base.Enter(owner);
    }

    public override void Execute(T owner)
    {
        base.Execute(owner);
    }

    public override void Exit(T owner)
    {
        base.Exit(owner);
    }
}