﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

public static class SaveManager
{
    private static string savePath = Application.dataPath + "/StreamingAssets/";

    public static void SaveData<T>(T t, string filename)
    {
        string json = JsonConvert.SerializeObject(t,Formatting.Indented);
        string file = savePath + filename + ".json";

        if (!File.Exists(file) || !Directory.Exists(savePath))
            Directory.CreateDirectory(savePath);

        if (File.Exists(file))
            File.WriteAllText(file, string.Empty);

        using (StreamWriter sw = File.AppendText(file))
            sw.Write(json);
    }

    public static void SaveEntity()
    {
        List<object> saveObject = new List<object>();

        foreach(KeyValuePair<int, Entity> entity in EntityManager.entities)
        {
            Entity e = entity.Value;
            if (e != null)
            {
                saveObject.Add(new object[]
                {
                e.transform.position,
                e.transform.rotation,
                //e.CurrentHealth,
                //e.MaxHealth,      moet nog even aangepast worden naar huidige health systeem.
                //e.Damage,
                });
            }
        }

        SaveData<List<object>>(saveObject, "save");
    }

    public static T LoadData<T>(string filename)
    {
        string file = savePath + filename + ".json";
        if (File.Exists(file))
        {
            string content = File.ReadAllText(file);
            T data = JsonConvert.DeserializeObject<T>(content);
            return data;
        }
        else
            return default(T);
    }


    //public static bool SaveData<T>(string key, T t, bool jsonSave = false)
    //{
    //    if (jsonSave || (jsonSave && typeof(T)!=typeof(string)))
    //        PlayerPrefs.SetString(key, JsonUtility.ToJson(t));
    //    else
    //        PlayerPrefs.SetString(key, (string)(object)t);
    //    return true;
    //}

    //public static T LoadData<T>(string key)
    //{
    //    if (PlayerPrefs.HasKey(key))
    //    {
    //        string s = PlayerPrefs.GetString(key);
    //        T data = JsonUtility.FromJson<T>(s);
    //        return data;
    //    }
    //    else
    //        return default(T);
    //}

    //public static bool SaveArray<T>(string key, T[] t)
    //{
    //    for (int i = 0; i < t.Length; i++)
    //        SaveData<T>(key + "_" + i, t[i],true);
    //    PlayerPrefs.SetString(key + "_length", t.Length.ToString());

    //    return true;
    //}

    //public static T[] LoadArray<T>(string key)
    //{
    //    int len = LoadData<int>(key + "_length");
    //    if (len != 0)
    //    {
    //        T[] array = new T[len];
    //        for (int i = 0; i < len; i++)
    //        {
    //            T o = LoadData<T>(key + "_" + i);
    //            array[i] = o;
    //        }
    //        return array;
    //    }
    //    else
    //        return default(T[]);
    //}
}
