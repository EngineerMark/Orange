﻿using System.Collections.Generic;

public class EntityManager : Manager {
    // Store entities in a dictionary
    public static Dictionary<int, Entity> entities = new Dictionary<int, Entity>();

    //// Adds an entity to the dictionary (called from Entity constructor)
    public static void AddEntity(Entity entity)
    {
        // instanceID + entity object
        entities.Add(entity.GetInstanceID(), entity);                             
    }

    // Returns entity if stored in dictionary, otherwise null
    public static Entity FindEntity(int instanceID)
    {
        // Check if the ID actually exists in dictionary
        if (entities.ContainsKey(instanceID))
            // If it does, return the Entity
            return entities[instanceID];
        // Otherwise return null
        return null;
    }

    public static T GetEntity<T>(int instanceID) where T : Entity
    {
        if (entities.ContainsKey(instanceID))
            return entities[instanceID] as T;
        return default(T);
    }
}
