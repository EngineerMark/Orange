﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public static PlaceBuilding placer;
    public int resources;
    public Resource resource;
    public Text test;

    public Manager[] managers;

    void Awake()
    {
        placer = GetComponent<PlaceBuilding>();
        JsonConvert.DefaultSettings().Converters.Add(new ColorConverter());

        managers = new Manager[]
        {
            new AssetManager(),
        };


        //SaveManager.SaveData<object[]>(test,"test");
        //object[] test2 = SaveManager.LoadData<object[]>("test");
    }

    public T GetManager<T>() where T : Manager
    {
        for (int i = 0; i < managers.Length; i++)
        {
            if (managers[i].GetType() == typeof(T))
                return managers[i] as T;
        }
        return default(T);
    }

    private void Start()
    {
        //SaveManager.SaveData<Dictionary<int, Entity>>(EntityManager.entities,"test");
        //SaveManager.SaveEntity();
    }

    public static PlaceBuilding Placer()
    {
        return placer;
    }

    public void Update()
    {
        if (resource.IsAdd == true)
        {
            resources += resource.totaalResources;
            resource.IsAdd = false;
        }


        test.text = Convert.ToString(resources);
    }
}
