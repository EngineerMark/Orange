﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]

[Serializable]
public class Item : ScriptableObject
{

    new public string name = "New Item";
    public Sprite icon = null;
    public bool isDefaultItem = false;
    public GameObject prefab;

    public Item()
    {

    }

    public virtual void Use()
    {
        //use the item
        PlaceBuilding.buildingPrefab = prefab;
        GameManager.Placer().CreateMoveableObject();
        //something might happen

        Debug.Log("Using " + name);
    }
}
