﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Inventory : MonoBehaviour
{
    [SerializeField]
    private int space = 8;
    public static Inventory instance;

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallBack;

    // Stores content of inventory
    [SerializeField]
    private List<Item> items;

    public Inventory()
    {
        // Create the actual object for list
        Items = new List<Item>();
    }

    private void Awake()
    {
        instance = this;
    }

    // Adds an item to the inventory
    public bool Add(Item item)
    {
        // Wether the passed argument is an actual object
        if (item != null)
        {
            if(items.Count >= space)
            {
                Debug.Log("Not enough room");
                return false;
            }

            // Add it
            Items.Add(item);

            // Log it to debug
            Debug.Log("Inserted " + item + " into " + this);

            if (onItemChangedCallBack != null)
                onItemChangedCallBack.Invoke();
        }

        return true;
    }

    // Adds an item to the inventory
    public void Insert(Item item)
    {
        // Wether the passed argument is an actual object
        if (item != null)
        {
            // Add it
            Items.Add(item);
            // Log it to debug
            Debug.Log("Inserted " + item + " into " + this);
        }
    }

    //public void Remove(Item item)
    //{
    //    Items.Remove(item);

    //    if (onItemChangedCallBack != null)
    //        onItemChangedCallBack.Invoke();
    //}

    // Get-setter for inventory content
    public List<Item> Items
    {
        get { return items; }
        set { items = value; }
    }
}
