﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Entity : MonoBehaviour
{
        // Add the entity to EntityManager
        //EntityManager.AddEntity(this);                      
    
    private float currentHealth;
    private float maxHealth;
    private float damage;
    private bool isSelected;

    private int poolID;

    private StateMachine<Entity> stateMachine;

    //[SerializeField]
    //GameObject DeathEffect;

    // Entity constructor
    public Entity()
    {
        maxHealth = 100f;
    }

        // Add the entity to EntityManager
        //EntityManager.AddEntity(this);         
    

    protected virtual void Awake()
    {     
        // Add the entity to EntityManager

        EntityManager.AddEntity(this);                          

        EntityManager.AddEntity(this);   
        currentHealth = maxHealth;

        // Initialize statemachine for the specific entity
        stateMachine = new StateMachine<Entity>(this);
        stateMachine.ChangeState(new IdleState<Entity>());
    }

    private void Update()
    {
    }


    public int PoolID
    {
        get
        {
            return poolID;
        }

        set
        {
            poolID = value;
        }
    }
}
