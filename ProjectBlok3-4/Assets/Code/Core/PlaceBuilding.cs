﻿using UnityEngine;
using System.Collections;

public class PlaceBuilding : MonoBehaviour
{
    // Becomes static later on, when making interface
    public static GameObject buildingPrefab;
    public float maxAngle = 5F;

    [SerializeField]
    private GameObject spawnedObject;

    [SerializeField]
    private Material allowedMaterial;
    [SerializeField]
    private Material disallowedMaterial;

    private Renderer _renderer;
    private Material originalMaterial;

    private bool allowedToPlace = false;
    public static bool isEditMode = false;

    private void Update()
    {
        if (spawnedObject != null)
        {
            isEditMode = true;
            // Create ray at point of cursor in camera
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            Debug.Log("Ray!");

            // Layermask
            int layerMask = 1 << spawnedObject.layer;
            layerMask = ~layerMask;

            // Create raycast into infinity from camera point with layermask
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
            {

                // Update position according to ray end position
                spawnedObject.transform.position = hitInfo.point + Vector3.up * spawnedObject.transform.lossyScale.y * 0.5f;
                // Rotate to the surface the object is on
                spawnedObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);

                // Maximal angle on which a building can be placed
                if (spawnedObject.transform.rotation.x * Mathf.Rad2Deg > maxAngle || spawnedObject.transform.rotation.x * Mathf.Rad2Deg < -maxAngle ||
                    spawnedObject.transform.rotation.y * Mathf.Rad2Deg > maxAngle || spawnedObject.transform.rotation.y * Mathf.Rad2Deg < -maxAngle ||
                    hitInfo.transform.gameObject.layer == 8)
                    allowedToPlace = false;
                else
                    allowedToPlace = true;
            }

            // Check for any other collisioned objects within the spawned object (temp one)
            Collider[] collisions = Physics.OverlapBox(spawnedObject.transform.position, spawnedObject.transform.lossyScale * 0.5f);

            // If there is any, disallow placement.
            foreach (Collider coll in collisions)
            {
                if (coll.gameObject.layer != spawnedObject.layer || coll.gameObject == spawnedObject)
                    continue;

                allowedToPlace = false;
                break;
            }

            // Apply material to fitting state
            if (allowedToPlace)
            {
                if (_renderer)
                    _renderer.material = allowedMaterial;
                // If LMB, create a new object that will be new spawned building
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    if (spawnedObject != null)
                    {
                        GameObject temp = Instantiate(spawnedObject);
                        temp.GetComponent<ItemPickup>().canPickUp = true;
                        temp.GetComponent<Renderer>().material = originalMaterial;
                    }
                }
            }
            else
                if (_renderer)
                _renderer.material = disallowedMaterial;
        }
        else
            isEditMode = false;
    }

    public void CreateMoveableObject()
    {
        if (spawnedObject == null)
        {
            spawnedObject = Instantiate(buildingPrefab);
            if (spawnedObject.GetComponent<ItemPickup>() != null)
                spawnedObject.GetComponent<ItemPickup>().canPickUp = false;
            spawnedObject.SetActive(true);
            _renderer = spawnedObject.GetComponent<Renderer>();
            originalMaterial = _renderer.material;
        }
        else
        {
            Destroy(spawnedObject);
            _renderer = null;
        }
    }
}
