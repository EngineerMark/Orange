﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour {

    public Item item;
    public bool canPickUp = true;

    public ItemPickup()
    {
        
    }

    private void Start()
    {
        PickUp();
    }

    //public void OnMouseDown()
    //{
    //    if (canPickUp)
    //        PickUp();
    //}

    void PickUp()
    {
        if (!PlaceBuilding.isEditMode)
        {
            //Debug.Log("Picking up" + item.name);
            bool wasPickedUp = Inventory.instance.Add(item);
            if (item.prefab == null)
            {
                item.prefab = Instantiate(gameObject); // Cloning rather than copying
                item.prefab.SetActive(false);
            }

            if (wasPickedUp)
                gameObject.SetActive(false);
        }
    }
}
