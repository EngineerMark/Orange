﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resource : MonoBehaviour {

    [SerializeField]
    public int totaalResources;
    private int addResources;
    public int randomAddResources;
    [SerializeField]
    public float timerToAddResources;
    [SerializeField]
    private Text recourceText;
    private int beginResources = 100;
    public bool IsAdd;

    public Text test;

    public void Start()
    {
        //totaalResources += beginResources;
    }

    public Resource()
    {
       
    }

    private void Update()
    {
        timerToAddResources -= Time.deltaTime;

        if (timerToAddResources <= 0)
        {
            totaalResources += randomAddResources;
            AddResources();
            IsAdd = true;
        }
        else
            IsAdd = false;

        recourceText.text = "Recource: " + totaalResources;
    }

    private void AddResources()
    {
        timerToAddResources = 10;
        randomAddResources = Random.Range(1, 20);
    }
}
