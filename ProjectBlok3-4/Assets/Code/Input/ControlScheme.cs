﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScheme {

    private Dictionary<InputAction, InputBinding> keyInput;

    public ControlScheme()
    {
        keyInput = new Dictionary<InputAction, InputBinding>();
    }

    public Dictionary<InputAction, InputBinding> KeyInput
    {
        get { return keyInput; }
        set { keyInput = value; }
    }
}
