﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private float speed = 30f;
    private float border = 10f;
    public Vector2 limit;
    private float scrollSpeed = 10f;
    private float minY = 5f;
    private float maxY = 75f;
    RaycastHit m_hit;
    [SerializeField] float Raylength = 200f;
    Camera viewCamera;
    Layer m_layerhit;


    public float Speed { get { return speed; } set { speed = value; } }

    public float Border { get { return border; } set { border = value; } }

    public float ScrollSpeed { get { return scrollSpeed; } set { scrollSpeed = value; } }

    public float MinY { get { return minY; } set { minY = value; } }

    public float MaxY { get { return maxY; } set { maxY = value; } }

    private void Start()
    {
        
        layerChangeObservers();
        viewCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        foreach (Layer layer in layerPriorities)
        {
            var hit = RaycastForLayer(layer);
            if (hit.HasValue)
            {
                m_hit = hit.Value;
                if(m_layerhit != layer)
                {
                    m_layerhit = layer;
                    layerChangeObservers();
                }
                m_layerhit = layer;
                return;
            }

        }
        m_hit.distance = Raylength;
        m_layerhit = Layer.RaycastEndStop;
        
    }
    public delegate void OnLayerChange();
    public OnLayerChange layerChangeObservers;

    

    public void Movement()
    {
        Vector3 pos = transform.position;

        if (Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - Border)
        {
            pos.z += Speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S) || Input.mousePosition.y <= Border)
        {
            pos.z -= Speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - Border)
        {
            pos.x += Speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A) || Input.mousePosition.x <= Border)
        {
            pos.x -= Speed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y += scroll * ScrollSpeed * 100f * Time.deltaTime;

        pos.x = Mathf.Clamp(pos.x, -limit.x, limit.x);
        pos.y = Mathf.Clamp(pos.y, MinY, MaxY);
        pos.z = Mathf.Clamp(pos.z, -limit.y, limit.y);

        transform.position = pos;
    }

    public RaycastHit hit
    {
        get { return m_hit; }
    }

    public Layer layerhit
    {
        get { return m_layerhit; }
    }

    public Layer[] layerPriorities =
   {
        Layer.Unit,
        Layer.Enemy,
        Layer.NotWalkable,
        Layer.Walkable
    };

    

    RaycastHit? RaycastForLayer(Layer layer)
    {
        int layerMask = 1 << (int)layer; 
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * m_hit.distance);

        RaycastHit hit; 
        bool hasHit = Physics.Raycast(ray, out hit, Raylength, layerMask);
        if (hasHit)
        {
            return hit;
        }
        return null;
    }
}
