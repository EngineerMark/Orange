﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : Entity {

    [SerializeField]
    private GameObject unitPrefab;

    private int isSpawning = 0;
    private float timer = 0;

    private float spawnTime = 10;

    private void FixedUpdate()
    {
        if (isSpawning == 1)
            timer += Time.fixedDeltaTime;

        if (timer / Time.maximumDeltaTime >= spawnTime)
        {
            // Spawn unit

            isSpawning = 0;
        }
    }

    private void OnMouseDown()
    {
        if (isSpawning != 1)
            isSpawning = 1;
    }
}
